package cn.com.micro;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.data.jpa.JpaRepositoriesAutoConfiguration;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;
import org.springframework.boot.autoconfigure.orm.jpa.HibernateJpaAutoConfiguration;
import org.springframework.cloud.client.circuitbreaker.EnableCircuitBreaker;
import org.springframework.cloud.openfeign.EnableFeignClients;
import org.springframework.transaction.annotation.EnableTransactionManagement;
/**
 * TODO 
 * @类名: MicroWebAppliction
 * @描述: 启动类) 
 * @时间: 2020年11月17日 下午5:48:01
 */
@SpringBootApplication(exclude={DataSourceAutoConfiguration.class,HibernateJpaAutoConfiguration.class,JpaRepositoriesAutoConfiguration.class})
public class MicroWebAppliction {
	private static final Logger logger = LoggerFactory.getLogger(MicroWebAppliction.class);
    public static void main( String[] args ){
    	//负责启动引导应用程序 
    	long startTime = System.currentTimeMillis();
    	SpringApplication.run(MicroWebAppliction.class, args);
    	logger.info("健康服务兑换系统healthy-service-web已完成启动,共花费：{}ms ",(System.currentTimeMillis()-startTime));
    }
}

	
