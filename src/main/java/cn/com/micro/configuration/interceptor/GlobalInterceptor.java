package cn.com.micro.configuration.interceptor;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.HandlerInterceptor;

//import cn.com.avivacofco.micro.online.domain.Store;
//import cn.com.avivacofco.micro.online.service.StoreService;

/**
 * 全局拦截器
 * 
 * @ClassName: GlobalInterceptor
 * @author evan
 * @date 2018年1月18日 下午5:11:04
 */
@Component
public class GlobalInterceptor implements HandlerInterceptor {
//	@Autowired
//	private StoreService storeService;
	@Override
	public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object o) throws Exception {
//		Store store = storeService.get();
//		String[] colorScheme = "#C3A769-#FFFFFF,#F3EEE1-#C3A769,#FFFFFF-#C3A769".split(",");
//		if(store != null){
//			colorScheme = store.getColorScheme().split(",");
//		}
//		request.setAttribute("firstBackColor", colorScheme[0].split("-")[0]);
//		request.setAttribute("firstFontColor", colorScheme[0].split("-")[1]);
//		request.setAttribute("secondBackColor", colorScheme[1].split("-")[0]);
//		request.setAttribute("secondFontColor", colorScheme[1].split("-")[1]);
//		request.setAttribute("thirdBackColor", colorScheme[2].split("-")[0]);
//		request.setAttribute("thirdFontColor", colorScheme[2].split("-")[1]);
		return true;
	}
}
