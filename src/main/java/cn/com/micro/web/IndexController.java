package cn.com.micro.web;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import cn.hutool.json.JSONObject;

@RestController
@RequestMapping("index")
public class IndexController {

	@RequestMapping("getUserList")
	@ResponseBody
	public String getUserList() {
		JSONObject json = new JSONObject();
		json.put("code", "200");
		json.put("data", "TOM");
		json.put("msg", "SUCCESS");
		return json.toString();
	}
}
