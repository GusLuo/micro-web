package cn.com.micro.configuration.directive;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;

import freemarker.core.Environment;
import freemarker.ext.beans.StringModel;
import freemarker.template.TemplateDirectiveModel;
import freemarker.template.TemplateModel;
import freemarker.template.TemplateModelException;
import freemarker.template.TemplateNodeModel;
import freemarker.template.TemplateScalarModel;

public abstract class BaseDirective  implements TemplateDirectiveModel{

	public  String getAttribute(String param,Map<String, TemplateModel> params) throws TemplateModelException{
		TemplateModel model = params.get(param);
		if(model == null) return null;
		if(model instanceof TemplateScalarModel){
			String value = ((TemplateScalarModel) model).getAsString();
			return value;
		}
		else{
			throw new TemplateModelException("参数获取失败");
		}
	}
	
	public Map<String, TemplateModel> addParamsToVariable(Environment env, Map<String, TemplateModel> params) throws TemplateModelException{
		Map<String, TemplateModel> origMap = new HashMap<String, TemplateModel>();
		if(params.size() <=0){
			return origMap; //元数据中无值
		}
		else{
			Set<Map.Entry<String, TemplateModel>> entrySet = params.entrySet();
			String key;
			TemplateModel value;
			for (Map.Entry<String, TemplateModel> entry : entrySet) {
				key = entry.getKey();
				value = env.getVariable(key);
				if (value != null) {
					origMap.put(key, value);
				}
				env.setVariable(key, entry.getValue());
			}
			return origMap;
		}
	}
	
	public void clearParamsToVariable(Environment env, Map<String, TemplateModel> params){
		Set<Map.Entry<String, TemplateModel>> entrySet = params.entrySet();
		String key;
		for (Map.Entry<String, TemplateModel> entry : entrySet) {
			key = entry.getKey();
			env.getCurrentNamespace().remove(key);
		}
	}

}
