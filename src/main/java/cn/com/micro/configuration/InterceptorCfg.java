package cn.com.micro.configuration;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurationSupport;

import cn.com.micro.configuration.interceptor.GlobalInterceptor;


@Configuration
public class InterceptorCfg extends WebMvcConfigurationSupport {
	@Autowired
	private GlobalInterceptor globalInterceptor;

	protected void addInterceptors(InterceptorRegistry registry) {
		super.addInterceptors(registry);
		registry.addInterceptor(globalInterceptor).addPathPatterns("/**/*.shtml").excludePathPatterns("/static/**");
	}

	@Override
	public void addResourceHandlers(ResourceHandlerRegistry registry) {
		registry.addResourceHandler("/staticweb/**").addResourceLocations("classpath:/static/");
	}

}