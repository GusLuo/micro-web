package cn.com.micro.configuration.filter;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.List;
import java.util.Properties;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class XssCheckFilter  implements Filter{
	private static final Properties XSS = new Properties();
	private static final Logger logger = LoggerFactory.getLogger(XssCheckFilter.class);
	private static final List<String> safeless = new ArrayList<String>();
	private static final List<String> ignorparams = new ArrayList<String>();
	
	public void init(FilterConfig filterConfig) throws ServletException {
		String keywords =filterConfig.getInitParameter("keyWords");
		String[] kws = keywords.split(",");
		for(String kw:kws){
			safeless.add(kw);
		}
		String params = filterConfig.getInitParameter("ignoreKeyWords");
		String[] pms = params.split(",");
		for(String pm:pms){
			ignorparams.add(pm);
		}
		
	}

	public void doFilter(ServletRequest req, ServletResponse resp,
			FilterChain chain) throws IOException, ServletException {
		    Enumeration params = req.getParameterNames(); 
	        HttpServletRequest request = (HttpServletRequest) req; 
	        HttpServletResponse response = (HttpServletResponse) resp; 
	                 
	        boolean isSafe = true; 
	        
	        if(isSafe){
	        	while (params.hasMoreElements()) { 
	        		String paramname = (String) params.nextElement();
	        		String cache = req.getParameter(paramname); 
	        		if(StringUtils.isNotBlank(cache)) { 
	        			if(!isignotparam(paramname)){
	        				if(!isSafe(cache)) { 
		        				isSafe = false; 
		        				break; 
		        			} 
	        			}
	        		} 
	        	} 
	        }
	        
	        
	        if(!isSafe){
	        	request.setAttribute("errMsg", "您提交的数据有非法字符，请输入正确的参数！");
	        	throw new ServletException("您输入的参数有非法字符，请输入正确的参数！");
	        }
	        else{
	        	 chain.doFilter(req, resp);
	        }
	        
	        
	}

	public void destroy() {
		
	}
	
	
	 
	private static boolean isSafe(String str) { 
        if(StringUtils.isNotBlank(str)) {     
            for (String s : safeless) { 
                if(str.toLowerCase().contains(s)) { 
					logger.info("====================================================================");
                	logger.info("出现非法字符:"+str+",----非法字符为："+s);
					logger.info("====================================================================");
                    return false; 
                } 
            } 
        } 
        return true; 
    } 
	
	private static boolean isignotparam(String param) { 
        if(StringUtils.isNotBlank(param)) {     
            for (String s : ignorparams) { 
                if(param.equals(s)) { 
                	logger.info("当前参数忽略校验:"+param);
                    return true; 
                } 
            } 
        } 
        return false; 
    } 

}
