package cn.com.micro.configuration.directive;

import static freemarker.template.ObjectWrapper.DEFAULT_WRAPPER;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import org.springframework.stereotype.Component;

import cn.com.micro.core.utils.JsonUtil;
import freemarker.core.Environment;
import freemarker.ext.beans.StringModel;
import freemarker.template.SimpleSequence;
import freemarker.template.TemplateDirectiveBody;
import freemarker.template.TemplateException;
import freemarker.template.TemplateModel;

@Component
public class JsonDirective extends BaseDirective{
	private static final String ATTR_OBJECT ="object";
	private static final String VARIABLE ="json";
	
	@Override
	public void execute(Environment env, Map params, TemplateModel[] loopVars, TemplateDirectiveBody body)
			throws TemplateException, IOException {
		TemplateModel model = (TemplateModel) params.get(ATTR_OBJECT);
		String json = null;
		if(model instanceof SimpleSequence){
			SimpleSequence m= (SimpleSequence)model;
			json = JsonUtil.toJson(m.toList());
		}
		else if(model instanceof StringModel){
			StringModel m =(StringModel)model;
			json = JsonUtil.toJson(m.getWrappedObject());
		}
		Map<String, TemplateModel> paramWrap = new HashMap<String, TemplateModel>(params);
		paramWrap.put(VARIABLE, DEFAULT_WRAPPER.wrap(json));//当前请求所有的参数
		Map<String, TemplateModel> origMap = this.addParamsToVariable(env, paramWrap);//所有参数放到上下文全局变量
		body.render(env.getOut());
		this.clearParamsToVariable(env, origMap);
	}

}
