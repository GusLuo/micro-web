package cn.com.micro.configuration;

import javax.annotation.PostConstruct;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.view.freemarker.FreeMarkerConfigurer;

//import cn.com.avivacofco.micro.online.service.impl.DatabaseTemplateLoader;
import freemarker.cache.MultiTemplateLoader;
import freemarker.cache.TemplateLoader;
import freemarker.template.TemplateModelException;

@Configuration
public class FreemarkerCfg {
	private static final Logger logger = LoggerFactory.getLogger(FreemarkerCfg.class);
	@Autowired
	private FreeMarkerConfigurer freeMarkerConfigurer;
//	@Autowired
//	private DatabaseTemplateLoader databaseTemplateLoader;
	
	//@Value("${aliyun.oss.bucketDomain}")
	//private String sctx ;
	@Value("${resource.static.domain}")
	private String sctx ;

	@PostConstruct
	public void freeMarkerConfigurer() throws TemplateModelException {
		freemarker.template.Configuration configuration = freeMarkerConfigurer.getConfiguration();
		
		//不要使用Lambda表达式 只有jdk1.8及以上版本支持
		//entries.forEach(entry -> configuration.setSharedVariable(entry.getKey(), entry.getValue()));
		//定义公共变量
		freeMarkerConfigurer.getConfiguration().setSharedVariable("sctx", sctx); //静态资源地址
		logger.info("初始静态资源地址，变量：{}，变量值：{}","sctx",sctx);
		
//		MultiTemplateLoader templateLoader = new MultiTemplateLoader(new TemplateLoader[]{databaseTemplateLoader});
//		configuration.setTemplateLoader(templateLoader);
	}


}
