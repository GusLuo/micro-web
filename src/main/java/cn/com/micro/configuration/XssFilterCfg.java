package cn.com.micro.configuration;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.web.servlet.FilterRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import cn.com.micro.configuration.filter.XssCheckFilter;


@Configuration
public class XssFilterCfg {
	@Value("${xss.keywords}")
	private String keyWords;
	@Value("${xss.ignore.keywords}")
	private String ignoreKeyWords;
	
	@Bean
	 public FilterRegistrationBean<XssCheckFilter> xssCheckFilterRegistration() {
	   FilterRegistrationBean<XssCheckFilter> registration = new FilterRegistrationBean<XssCheckFilter>();
	   registration.setFilter(new XssCheckFilter());
	   registration.addUrlPatterns("*.shtml");
	   registration.addInitParameter("keyWords", keyWords);
	   registration.addInitParameter("ignoreKeyWords", ignoreKeyWords);
	   registration.setName("xssCheckFilter");
	   registration.setOrder(1);
	   return registration;
	 }
	
}
