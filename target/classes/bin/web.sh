#!/bin/sh
APP_NAME=micro-web
		

usage() {
    echo "Usage: sh eju-micro-app.sh [start|stop|check]"
    exit 1
}

kills(){
    tpid=`ps -ef|grep $APP_NAME|grep -v grep|grep -v kill|awk '{print $2}'`
    if [[ $tpid ]]; then
        echo 'Kill Process!'
        kill -9 $tpid
    fi
}

start(){
    rm -f tpid
    nohup java -Xms128m -Xmx512m  -jar $APP_NAME.jar >/dev/null 2>&1 &
    echo $! > tpid
    echo $!
}

stop(){
        tpid1=`ps -ef|grep $APP_NAME|grep -v grep|grep -v kill|awk '{print $2}'`
    echo tpid1-$tpid1
        if [[ $tpid1 ]]; then
        echo 'Stop Process...'
        kill -15 $tpid1
    fi
    sleep 5
    tpid2=`ps -ef|grep $APP_NAME|grep -v grep|grep -v kill|awk '{print $2}'`
        echo tpid2-$tpid2
    if [[ $tpid2 ]]; then
        echo 'Kill Process!'
        kill -9 $tpid2
    else
        echo 'Stop Success!'
    fi

}

check(){
    tpid=`ps -ef|grep $APP_NAME|grep -v grep|grep -v kill|awk '{print $2}'`
    if [[ tpid ]]; then
        echo 'is running.'
    else
        echo 'is not running.'
    fi
}



case "$1" in
    "start")
        start
        ;;
    "stop")
        stop
        ;;
    "check")
        check
        ;;
    "kill")
        kills
        ;;
    *)
        usage
        ;;
esac